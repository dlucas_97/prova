# Mòdul de programació

Aquest es un repositori de programació amb llenguatge C

# Índex

- ### [El tutorial amb MarkDown](#tutorial-amb-markdown)
- ### [Introducció.](#introducció)
- ### [Programació en Java](#programació-java)

## Tutorial amb MarkDown
---
[youtube] (https://www.youtube.com/)
![MarkDown](imatges/markdown.png) 

<br>
<br>
<br>

<div style="text-align: right">

### [Tornar a l'Index...](#índex)

</div>

# Programació JAVA

## Introducció
----
<br>
<br>

* primer
    * primer 
        * primer 
* segon
* tercer
<br>

1. primer
    1. primer

1. segon
<br>

**això està en negreta** 
<br>

_està en cursiva_

```js
//aquest es un comentari
a = 5
var pi = 32
```
per posar mes espais &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; new blanc space

<br>

<br>

### Taules
---
| Nom | Cognoms | Adreça | Edat |
| --- | --- | --- | --- | 
|David| Lucas| Nogerasss | 25 |
|David|Lucas|ADF|21|
|David|Lucasasdasda|JKÑ|ad

# Menus
